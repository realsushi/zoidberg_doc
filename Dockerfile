FROM node:14.9.0
WORKDIR /app
COPY . .
RUN npm install hexo-cli -g \
&& npm install hexo-renderer-scss \
&& test -e package.json && npm install
CMD ["hexo", "generate"]